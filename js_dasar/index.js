let input1DOM = document.getElementById("input1")
let input2DOM = document.getElementById("input2")

function tambah() {
    let hasilLuas = parseFloat(input1DOM.value) + parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilLuas
}
function kurang() {
    let hasilLuas = parseFloat(input1DOM.value) - parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilLuas
}
function kali() {
    let hasilLuas = parseFloat(input1DOM.value) * parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilLuas
}
function bagi() {
    let hasilLuas = parseFloat(input1DOM.value) /  parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilLuas
}
function mod() {
    let hasilLuas = parseFloat(input1DOM.value) % parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilLuas
}

function pangkat() {
    let hasilLuas = parseFloat(input1DOM.value) ** parseFloat(input2DOM.value)
    document.getElementById('hasil').value = hasilLuas
}

function akar() {
    let hasilLuas = Math.sqrt(parseFloat(input1DOM.value))
    document.getElementById('hasil').value = hasilLuas
}
function reset() {
    input1.value = ""
    input2DOM.value = ""

    location.reload();
}